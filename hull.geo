Merge "hull.iges";
SetFactory("OpenCASCADE");

bb() = BoundingBox Surface {1};

Point(3)  = {bb(0) - 10, bb(1) - 10, bb(2) - 10};
Point(4)  = {bb(3) + 10, bb(1) - 10, bb(2) - 10};
Point(5)  = {bb(0) - 10, bb(4)     , bb(2) - 10};
Point(6)  = {bb(3) + 10, bb(4)     , bb(2) - 10};
Point(7)  = {bb(0) - 10, bb(1) - 10, bb(5)     };
Point(8)  = {bb(3) + 10, bb(1) - 10, bb(5)     };
Point(9)  = {bb(0) - 10, bb(4)     , bb(5)     };
Point(10) = {bb(3) + 10, bb(4)     , bb(5)     };

Line(5)  = {1, 9};
Line(6)  = {2, 10};
Line(7)  = {3, 4};
Line(8)  = {5, 6};
Line(9)  = {7, 8};
Line(10) = {3, 7};
Line(11) = {4, 8};
Line(12) = {5, 9};
Line(13) = {6, 10};
Line(14) = {3, 5};
Line(15) = {4, 6};
Line(16) = {7, 9};
Line(17) = {8, 10};

Curve Loop(2) = {5, 4, 6, 13, 8, 12};
Curve Loop(3) = {9, 11, 7, 10};
Curve Loop(4) = {5, 2, 6, 17, 9, 16};
Curve Loop(5) = {8, 14, 7, 15};
Curve Loop(6) = {13, 15, 11, 17};
Curve Loop(7) = {12, 16, 10, 14};

Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(4) = {4};
Plane Surface(5) = {5};
Plane Surface(6) = {6};
Plane Surface(7) = {7};

Surface Loop(1) = {1, 2, 3, 4, 5, 6, 7};

Volume(1) = {1};

Physical Surface("CenterLine") = {4};
Physical Surface("Outlet") = {7};
Physical Surface("Sides") = {5, 3};
Physical Surface("Inlet") = {6};
Physical Surface("Top") = {2};
Physical Surface("Hull") = {1};

Physical Volume("Water") = {1};

Mesh.CharacteristicLengthFactor = 0.4;
