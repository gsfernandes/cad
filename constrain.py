class aft:
    def __new__(cls, array):
        cls.x(array)
        cls.y(array)
        cls.z(array)
        return array

    @staticmethod
    def x(array):
        array[:, 0, 0] = -1
        array[:, -1, 0] = 0

    @staticmethod
    def y(array):
        array[:, 0, 1] = 0
        array[-1, :, 1] = 0
        array[0, -1, 1] = -1

    @staticmethod
    def z(array):
        array[:, 0, 2] = 0
        array[0, :, 2] = 0
        array[-1, -1, 2] = -1


class forward:
    def __new__(cls, array):
        cls.x(array)
        cls.y(array)
        cls.z(array)
        return array

    @staticmethod
    def x(array):
        array[:, -1, 0] = 1
        array[:, 0, 0] = 0

    @staticmethod
    def y(array):
        array[:, -1, 1] = 0
        array[-1, :, 1] = 0
        array[0, 0, 1] = -1

    @staticmethod
    def z(array):
        array[:, -1, 2] = 0
        array[0, :, 2] = 0
        array[-1, 0, 2] = -1
