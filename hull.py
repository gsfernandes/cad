import OCC
import numpy as np
from . import constrain
from abc import ABC, abstractmethod
from OCC.Core.Geom import Geom_BezierSurface as Bezier
from OCC.Core.gp import gp_Pnt as Point
from OCC.Core.TColgp import TColgp_Array2OfPnt as PointArray2D
from OCC.Core.TColGeom import TColGeom_Array2OfBezierSurface as BezierArray
from OCC.Core.IGESCAFControl import IGESCAFControl_Writer as IGESWriter

# Display import:
from OCC.Display.SimpleGui import init_display


def hull(profile, length, breadth, draught):
    Hull.write(Hull(profile, length, breadth, draught))
    return True


def Points(li):
    if isinstance(li[0], (list, np.ndarray)):
        return [Points(el) for el in li]
    else:
        return Point(*li)


# disp, _, _, _ = init_display()


def display(obj, disp):
    disp.DisplayShape(obj, update=True)


def _listCast2D(li, type):
    castedList = type(0, len(li[0]) - 1, 0, len(li) - 1)
    for col_num, col in enumerate(li):
        for row_num, elem in enumerate(col):
            castedList.SetValue(row_num, col_num, elem)
    return castedList


class Hull:
    def __new__(cls, array, length, breadth, draught):
        assert array.shape[1] >= 4, "The axial point number must be 4 or above"
        split = array.shape[1] // 2

        # Make sure there's a common line 'tween fore and aft
        aft_array, forward_array = array[:, : split + 1], array[:, split:]
        aft_array = cls._numpyPointArrayAft(aft_array)
        forward_array = cls._numpyPointArrayForward(cls, forward_array)
        # print("\n\n\n", aft_array[:, :, 1], "\n\n", forward_array[:, :, 1])
        aft_array = constrain.aft(aft_array)
        forward_array = constrain.forward(forward_array)
        # print("\n\n\n", aft_array[:, :, 1], "\n\n", forward_array[:, :, 1])

        array = np.concatenate((aft_array, forward_array), 1)
        # print("\n\n\n", array[:, :, 1])
        array[:, :, 0] = cls._scale(array[:, :, 0], -length / 2, length / 2)
        array[:, :, 1] = cls._scale(array[:, :, 1], -breadth / 2, 0)
        array[:, :, 2] = cls._scale(array[:, :, 2], -draught, 0)

        # print("\n\n\n", array[:, :, 1], "\n")

        array = cls._OCCPointArray(array)
        return Bezier(array)

    @staticmethod
    def _indices(rows, cols):
        rows = np.arange(rows)
        cols = np.arange(cols)
        indices = np.empty((len(rows), len(cols), 2), dtype=np.int)
        indices[..., 0] = rows[:, None]
        indices[..., 1] = cols
        return indices

    @classmethod
    def _numpyPointArray(cls, array, x_indices=None, y_indices=None):
        rows, cols = array.shape
        indices = cls._indices(rows, cols)
        x = (
            x_indices
            if x_indices is not None
            else indices[:, :, 1].reshape([rows, cols, 1])
        )
        y = (
            y_indices
            if y_indices is not None
            else -indices[:, :, 0].reshape([rows, cols, 1])
        )
        array = np.concatenate((x, y, array.reshape([rows, cols, 1])), -1)
        array[:, :, 0] = np.sort(array[:, :, 0])
        array[:, :, 1] = np.sort(array[:, :, 1], 0)[::1]
        array[:, :, 2] = cls._scale(array[:, :, 2], -1, 0)
        # ToDo: Replace this with cls._scale:
        array[:, :, 0] = array[:, :, 0] / (array.shape[0] - 1)
        array[:, :, 1] = array[:, :, 1] / (array.shape[1] - 1)
        return array

    @classmethod
    def _numpyPointArrayAft(cls, array):
        rows, cols = array.shape
        x_indices = -1 * cls._indices(rows, cols)[:, ::-1, 1].reshape([rows, cols, 1])
        return cls._numpyPointArray(array, x_indices)

    def _numpyPointArrayForward(cls, array):
        rows, cols = array.shape
        x_indices = cls._indices(rows, cols)[:, :, 1].reshape([rows, cols, 1])
        return cls._numpyPointArray(array, x_indices)

    @staticmethod
    def _OCCPointArray(array):
        return _listCast2D(Points(array), PointArray2D)

    @staticmethod
    def _scale(array, min, max):
        array = np.subtract(array, array.min())
        array = array / array.max() - array.min()
        array = array * (max - min)
        array = array + min
        assert array.min() == min
        assert array.max() == max
        return array

    @staticmethod
    def write(geom, file_name="hull.iges"):
        igeswriter = IGESWriter()
        igeswriter.AddGeom(geom)
        igeswriter.Write(file_name)


if __name__ == "__main__":
    Hull.write(Hull(np.random.random([5, 5]), 100, 30, 20))
